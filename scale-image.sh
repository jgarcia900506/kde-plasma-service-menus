#!/bin/bash

output_format='png'

file_name=${1%.*}
file_extension=${1##*.}

can_resize=1
if [ $file_extension == $output_format ]; then
  file_timestamp=$(stat -c %Y "$1")
  current_timestamp=$(date +%s)
  duration=$((current_timestamp - file_timestamp))

  days=$((duration / 86400))
  hours=$(( (duration % 86400) / 3600 ))
  minutes=$(( (duration % 3600) / 60 ))
  seconds=$((duration % 60))

  total=$minutes
  total=$((total + (hours * 60)))
  total=$((total + (days * 24 * 60)))

  echo ":: $total"
  if [ $total -lt 10 ]; then
    can_resize=0;
  fi
 
  echo ":: $days days, $hours hours, $minutes minutes, $seconds seconds"
fi



if [ $can_resize -eq 1 ]; then

  waifu2x-ncnn-vulkan -i "${1}" -n $2 -s $3 -f $output_format -o "${file_name}.${output_format}"

  if [[ $? -eq 0 && $file_extension != $output_format ]]; then
    rm -v "${1}"
  fi

fi
